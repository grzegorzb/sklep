<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 23.08.2020
  Time: 09:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.*" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <title>Orders</title>
</head>
<body>
<%! private ArrayList<String> orders = new ArrayList<String>
        (Arrays.asList("product1", "product2", "product3")); %>
<% request.setAttribute("orders", orders);%>
<% for (String product : orders) {
    response.getWriter().println(product + "<br/>");
}%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:forEach var="i" begin="0" end="${orders.size() - 1}">
    Item <c:out value="${orders.get(i)}"/> <br>
</c:forEach>
</body>
</html>


